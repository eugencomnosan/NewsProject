import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Rx";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { JwtUtilsService } from "./jwt-utils.service";
import { Page } from "../models/Page";
import { MenuItem } from "../models/MenuItem";

@Injectable()
export class MenuItemService {
  private readonly path = "api/vesti";
  constructor(private http: HttpClient) { }

  getAll(page: number, itemsPerPage: number,name:string,catId:number): Observable<Page<MenuItem>> {
    let params = new HttpParams();
    params = params.append("page", page.toString());
    params = params.append("size", itemsPerPage.toString());
    params = params.append("name",name);
    params = params.append("id",catId.toString());

    return this.http.get(this.path, { params: params }) as Observable<Page<MenuItem>>;
  }


  get(id: number): Observable<MenuItem> {
    return this.http.get(`${this.path}/${id}`) as Observable<MenuItem>;
  }

  save(item: MenuItem): Observable<MenuItem> {
    let params = new HttpParams();
    params = params.append("Content-Type", "application/json");
    return item.id === undefined ?
      this.http.post(this.path, item, { params }) as Observable<MenuItem>
      : this.http.put(`${this.path}/${item.id}`, item, { params }) as Observable<MenuItem>;

  }

  delete(id: number): any {
    return this.http.delete(`${this.path}/${id}`);
  }
}
