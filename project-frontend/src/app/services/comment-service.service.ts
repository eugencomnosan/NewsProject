import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Rx";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { JwtUtilsService } from "./jwt-utils.service";
import { Page } from "../models/Page";
import { Category } from "../models/Category";
import { Comment } from "../models/Comments";
import { HttpParams } from "@angular/common/http";

@Injectable()
export class CommentService {
    private readonly path = "api/comments";
    constructor(private http: HttpClient) { }

    getAll(): Observable<Comment[]> {
        return this.http.get(this.path) as Observable<Comment[]>;
    }

    saveComment(comment: Comment): Observable<Comment> {
        let params = new HttpHeaders();
        params = params.append('Content-Type', 'application/json');
  
        return this.http.post(this.path, comment, { headers:params }) as Observable<Comment>;
  
      }
}
