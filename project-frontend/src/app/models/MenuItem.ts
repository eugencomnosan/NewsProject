import { Category } from "./Category";
import {Comment} from "./Comments"


export interface MenuItem {
    id?: number;
    category: Category;
    naslov: string;
    opis: string;
    sadrzaj: string;
    comments?:Comment[]  ;          
}