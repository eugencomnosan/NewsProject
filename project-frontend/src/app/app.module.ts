import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
// import { HttpModule } from "@angular/http";
import { RouterModule, Routes } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";

import { AppComponent } from "./app.component";
import { LoginComponent } from "./components/login/login.component";
import { RegisterComponent } from "./components/register/register.component";
import { Add } from "./components/add/add.component";


import { AuthenticationService } from "./services/authentication-service.service";
import { JwtUtilsService } from "./services/jwt-utils.service";
import { TokenInterceptorService } from "./services/token-interceptor.service";
import { CanActivateAuthGuard } from "./services/can-activate-auth.guard";
import { MenuItemService } from "./services/menu-item-service.service";
import { CategoryService } from "./services/category-service.service";
import { MenuItemListComponent } from "./components/menu-item-list/menu-item-list.component";
import { Edit } from "./components/edit/edit.component";
import { FilterComponent } from "./components/filter/filter.component";
import { VestComponent } from './components/vest/vest.component';
import { AddUpdateComponent } from './components/add-update/add-update.component';
import { CommentService } from "./services/comment-service.service";


const appRoutes: Routes = [
  { path: "login", component: LoginComponent },
  { path: "register", component: RegisterComponent },
  { path: "vesti", component: MenuItemListComponent },
  { path: "vesti/:id", component: VestComponent },
  { path: "add", component: Add, canActivate: [CanActivateAuthGuard] },
  { path: "edit/:id", component: Edit, canActivate: [CanActivateAuthGuard] },
  { path: "", redirectTo: "menu", pathMatch: "full" },
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    MenuItemListComponent,
    Add,
     Edit,
    FilterComponent,
    VestComponent,
    AddUpdateComponent
     
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes,
      {
        enableTracing: false
      }
    )
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    },
    AuthenticationService,
    CanActivateAuthGuard,
    JwtUtilsService,
    CategoryService,
    MenuItemService,
    CommentService
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
