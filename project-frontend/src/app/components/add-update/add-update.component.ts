import { Component, OnInit,EventEmitter } from '@angular/core';
import { MenuItem } from '../../models/MenuItem';
import { Input, Output } from '@angular/core';
import { Category } from '../../models/category';
import { CategoryService } from '../../services/category-service.service';

@Component({
  selector: 'app-add-update',
  templateUrl: './add-update.component.html',
  styleUrls: ['./add-update.component.css']
})
export class AddUpdateComponent implements OnInit {

  @Input()
  item:MenuItem;

  @Output()
  addItemEmitter:EventEmitter<MenuItem> = new EventEmitter();

  categories:Category[];


  constructor(private categoryService:CategoryService) { }

  ngOnInit() {
    this.loadCategories();
  }
  add(){
    this.addItemEmitter.next(this.item);
    this.item = {
      category: null,
      naslov: "",
      opis: "",
      sadrzaj: "",
    }
  }

  reset(){
    this.item = {
      category: null,
      naslov: "",
      opis: "",
      sadrzaj: "",
    }
  }

  byId(cat1:Category, cat2:Category){
    if(cat1 && cat2){
      return cat1.id===cat2.id;
    }
  }
  loadCategories(){
  this.categoryService.getAll().subscribe(
    (categories)=>{
      this.categories = categories;
    });
}
}
