import { Component, OnInit } from '@angular/core';
import { MenuItem } from '../../models/MenuItem';
import { ActivatedRoute } from '@angular/router';
import { CategoryService } from '../../services/category-service.service';
import { MenuItemService } from '../../services/menu-item-service.service';
import { Comment } from '../../models/Comments';
import { CommentService } from '../../services/comment-service.service';

@Component({
  selector: 'app-vest',
  templateUrl: './vest.component.html',
  styleUrls: ['./vest.component.css']
})
export class VestComponent implements OnInit {

  private id: number;
  private vest: MenuItem;
  private newComment:Comment;
  


  constructor(private route: ActivatedRoute,
    private categoryService: CategoryService,
    private menuItemService: MenuItemService,
    private commentService:CommentService) { }

  ngOnInit() {
    this.resetComment();
    this.resetProject();
    this.loadData();
    this.loadComment();
  }

  loadData() {
    this.route.params.subscribe(param => {
      this.id = +param['id'];
      this.menuItemService.get(this.id).subscribe(
        (data) => {
          this.vest = data;
        }
      )
    });
  }
  loadComment(){
    this.commentService.getAll().subscribe(
      (data) => {
        this.vest.comments = data;
  });
}

  resetProject() {
    this.vest = {
      category: null,
      naslov: "",
      opis: "",
      sadrzaj: "",
      comments:null
    }
  }
  
  resetComment() {
    this.newComment = {
      id: 0,
      text: '',
      name:''
    }
  }
  // addPledge() {
  //   this.pledgeService.savePledge(this.newPledge).subscribe(
  //     (pledge) => {
  //       this.project.pledges.push(pledge);
  //       this.projectService.saveProject(this.project);//update projekat u bazi
  //       this.collectedAmount += pledge.amount;
  //       this.loadData();
  //     }
  //   )
  //   this.resetPledge();
  // }
  addComment() {
    this.commentService.saveComment(this.newComment).subscribe(
      (comment) => {
        this.vest.comments.push(comment);
        this.menuItemService.save(this.vest).subscribe(()=> this.loadComment());
      }
    )
  
    this.resetComment();
  }
  // addComment() {
  //   this.commentService.saveComment(this.newComment).subscribe(
  //     (comment) => {
  //       this.project.comments.push(comment);
  //       this.projectService.saveProject(this.project);//update projekat u bazi
  //       this.loadData();
  //     }
  //   )
  //   this.resetComment();
  }


