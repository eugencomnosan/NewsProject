import { Component as c, OnInit } from "@angular/core";

import { AuthenticationService } from "../../services/authentication-service.service";
import { Observable } from "rxjs/Observable";
import { Input } from "@angular/core";
import { Output } from "@angular/core";
import { EventEmitter } from "@angular/core";

import { Router } from "@angular/router";
import { Category } from "../../models/category";
import { Page } from "../../models/Page";
import { MenuItem } from "../../models/MenuItem";
import { CategoryService } from "../../services/category-service.service";
import { MenuItemService } from "../../services/menu-item-service.service";


@c({
  selector: "app-add",
  templateUrl: "./add.component.html",
  styleUrls: ["./add.component.css"]
})
export class Add {
  item: MenuItem;

  dataLoaded = false;
  categories: Category[];

  constructor(
    private categoryService:CategoryService,
    private menuItemService:MenuItemService,
    private router: Router) { }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.categoryService.getAll().subscribe(
      (data) => {
        this.categories = data;
        this.dataLoaded = true;
      },
      (error) => {
        console.log(error);
      }
    );

    this.item = {
      category: null,
      naslov: "",
      opis: "",
      sadrzaj: "",
    };
  }
  add() {
    this.menuItemService.save(this.item).subscribe(
      (yes) => {
      },
      (no) => {
        alert("Error");
      }
    );

    this.item = {
      category: null,
      naslov: "",
      opis: "",
      sadrzaj: "",
    };
    const res = confirm("Press ok to add more or cancel to go back to the list?");
    if (!res) {
      this.router.navigate(["/vesti"]);
    }
  }

  byId(cat1: Category, cat2: Category) {
    if (cat1 && cat2) {
      return cat1.id === cat2.id;
    }
  }

}

