package project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import project.dto.ResponseDTO;
import project.model.Comment;
import project.service.CommentService;
import project.service.VestService;
@RestController
public class CommentController {
	@Autowired
	CommentService commentService;
	@Autowired
	VestService vestService;
	



	@GetMapping(value = "api/comments")
	public ResponseEntity<List<Comment>> get() {
		final List<Comment> comments = commentService.findAll();
		return new ResponseEntity<>(comments, HttpStatus.OK);
	}

	@PostMapping(value = "api/comments")
	public ResponseEntity<Comment> create(@RequestBody Comment recComment) {
		Comment comment = new Comment();

		comment.setText(recComment.getText());
		comment.setName(recComment.getName());

		
		comment = commentService.save(comment);
		
		return new ResponseEntity<>(comment, HttpStatus.CREATED);
	}

	@PreAuthorize("hasAnyAuthority('ADMINISTRATOR')")
	@DeleteMapping(value = "/api/comment_delete/{id}")
	public ResponseEntity<ResponseDTO> delete(@PathVariable Long id) {
		Comment comment = commentService.findOne(id);

		if (comment != null) {
			commentService.remove(id);
			return new ResponseEntity<>(new ResponseDTO("OK"), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new ResponseDTO("NOT FOUND"), HttpStatus.NOT_FOUND);
		}
	}
}
