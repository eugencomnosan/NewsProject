package project.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import project.dto.ResponseDTO;
import project.model.Vest;
import project.service.VestService;
@RestController
public class VestController {
	@Autowired
	VestService vestService;

	@GetMapping(value = "/api/vesti")
	public ResponseEntity<Page<Vest>> getMenuItemsPage(Pageable page,String name,Long id) {
		Page<Vest> menuItems = vestService.findAll(page,name,id);
		

		return new ResponseEntity<Page<Vest>>(menuItems, HttpStatus.OK);
	}

	@GetMapping(value = "/api/vesti/{id}")
	public ResponseEntity<Vest> getOne(@PathVariable Long id) {
		Vest project = vestService.findOne(id);
		return Optional.ofNullable(project).map(p -> new ResponseEntity<>(project, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));

	}

	@PostMapping(value = "/api/vesti")
	public ResponseEntity<Vest> create(@RequestBody Vest recItem) {

		Vest item = new Vest();
		item.setNaslov(recItem.getNaslov());
		item.setOpis(recItem.getOpis());
		item.setSadrzaj(recItem.getSadrzaj());
		item.setCategory(recItem.getCategory());
		item.setComments(recItem.getComments());
	

		return new ResponseEntity<>(vestService.save(item), HttpStatus.CREATED);
	}

	@PreAuthorize("hasAnyAuthority('Admin')")
	@DeleteMapping(value = "/api/vesti/{id}")
	public ResponseEntity<ResponseDTO> delete(@PathVariable Long id) {
		Vest item = vestService.findOne(id);

		if (item != null) {
			vestService.delete(id);
			return new ResponseEntity<>(new ResponseDTO("OK"), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new ResponseDTO("NOT FOUND"), HttpStatus.NOT_FOUND);
		}
	}

	@PutMapping(value = "/api/vesti/{id}")
	public ResponseEntity<Vest> update(@PathVariable Long id, @RequestBody Vest recItem) {
		Vest item = vestService.findOne(id);

		if (item != null) {
			
			item.setNaslov(recItem.getNaslov());
			item.setOpis(recItem.getOpis());
			item.setSadrzaj(recItem.getSadrzaj());
			item.setCategory(recItem.getCategory());
			item.setComments(recItem.getComments());

			vestService.save(item);
			return new ResponseEntity<Vest>(item, HttpStatus.OK);
		} else {
			return new ResponseEntity<Vest>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping(value = "/api/vesti/category/{catId}")
	public ResponseEntity<List<Vest>> getMenuItemsByCategory(@PathVariable Long catId) {
		List<Vest> menuItems = vestService.findByCategoryId(catId);
		return Optional.ofNullable(menuItems).map(p -> new ResponseEntity<>(menuItems, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));

	}
	
	
}
