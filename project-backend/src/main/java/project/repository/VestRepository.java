package project.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import project.model.Vest;

@Component
public interface VestRepository extends JpaRepository<Vest, Long> {
	public List<Vest> findByCategoryId(Long id);
	public Page<Vest> getByNaslovContains(Pageable page,String name);
	public Page<Vest> getByNaslovContainsAndCategoryId(Pageable page,String name,Long id);
}
