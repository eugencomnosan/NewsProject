package project.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Comment {
	@Id
	@GeneratedValue
	private Long id;
	private String name;
	private String text;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Vest vest;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}


		
	public Comment(Long id, String name, String text) {
		super();
		this.id = id;
		this.name = name;
		this.text = text;
	}
	public Comment() {
		
	}
	
	
	
}
