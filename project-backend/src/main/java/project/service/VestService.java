package project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import project.model.Vest;
import project.repository.VestRepository;

@Component
public class VestService {
	@Autowired
	VestRepository VestRepository;

	public List<Vest> findAll() {
		return VestRepository.findAll();
	}

	public Page<Vest> findAll(Pageable page, String name,Long id) {
		if(id ==0 )
		return VestRepository.getByNaslovContains(page, name);
		else {
			return  VestRepository.getByNaslovContainsAndCategoryId(page, name, id);
		}
	}

	public Vest findOne(Long id) {
		return VestRepository.findOne(id);

	}

	public void delete(Long id) {
		VestRepository.delete(id);
	}

	public Vest save(Vest vest) {
		return VestRepository.save(vest);
	}

	public List<Vest> findByCategoryId(Long catId) {
		return VestRepository.findByCategoryId(catId);
	}
	

	// public List<MenuItem> getByNameContains(String name) {
	// return menuItemRepository.getByNameContains(name);
	// }

}
