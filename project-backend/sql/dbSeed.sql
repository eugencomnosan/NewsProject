CREATE DATABASE project;
USE project;

insert into category (id, name) values (1,"Sport");
insert into category (id, name) values (2,"Politika");


insert into vest(naslov,opis,sadrzaj,category_id) values ("153. Derbi","U subotu se igra derbi","Igraci partizana i zvezde igraju u subotu od 16h 153. derbi",1);
insert into vest(naslov,opis,sadrzaj,category_id) values ("158. Derbi","U subotu se igra derbi","Igraci partizana i zvezde igraju u subotu od 16h 153. derbi",1);
insert into vest(naslov,opis,sadrzaj,category_id) values ("158. El clasico","U subotu se igra derbi","Igraci barcelone i reala  igraju u subotu od 16h 158. derbi",1);
insert into vest(naslov,opis,sadrzaj,category_id) values (" FOTO VIDEO","Srbija pobedila ","Fudbaleri srbije izgubili rezultatom 5:0 od reprezentacije Gane , selektor kaze da smo ipak mi moralni pobednici",1);
insert into vest(naslov,opis,sadrzaj,category_id) values ("Skupstina zaseda","U petak je prvi sastanak skupstine","Odbornici skupstine vidjeni kako su zaseli u poznatoj kafani u Skadarliji",2);
insert into vest(naslov,opis,sadrzaj,category_id) values ("Nemam vise ideja","Ovde je opis ","Ovde bi trebao biti sadrzaj :) ",2);

insert into comment(name,text) values ("Pera","Zdravo ja sam Pera");
insert into comment(name,text) values ("Pera","Zdravo i ja sam Pera");

insert into vest_comments(vest_id,comments_id) values (1,1);
insert into vest_comments(vest_id,comments_id) values (1,2);
-- insert users
-- password is 12345 (bcrypt encoded) 
insert into security_user (username, password ,role) values 
	('admin', '$2a$04$4pqDFh9SxLAg/uSH59JCB.LwIS6QoAjM9qcE7H9e2drFuWhvTnDFi', 'Admin');
-- password is abcdef (bcrypt encoded)
insert into security_user (username, password,  role) values 
	('stanar', '$2a$04$Yr3QD6lbcemnrRNLbUMLBez2oEK15pdacIgfkvymQ9oMhqsEE56EK', 'User');
